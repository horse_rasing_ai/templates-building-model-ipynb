from numpy.lib import utils
import pandas as pd
import MySQLdb
import numpy as np
import tensorflow as tf
from tensorflow._api.v2 import data
from . import utilities as utils
import logging

# ログ設定
logging.basicConfig(filename='./log/sim_result.log', level=logging.INFO)

def get_result_odds(race_id):
    # 接続する 
    con = MySQLdb.connect(
            user='user',
            passwd='secret',
            host='127.0.0.1',
            db='keiba_db',
            charset="utf8")
 
    # カーソルを取得する
    cur= con.cursor()

    # クエリを実行する
    sql = "select * from t_race_info where id = '{}'".format(race_id)
    cur.execute(sql)

    # 実行結果をすべて取得する
    rows = cur.fetchall()
 
    cur.close()
    con.close()
    
    return rows

def process_odds_data(data):
    outputs = {
        'tansho': data[13],
        'fukusho_1st': data[14],
        'fukusho_2nd': data[15],
        'fukusho_3rd': data[16],
        'wakuren': data[17],
        'umaren': data[18],
        'wide_1st2nd': data[19],
        'wide_1st3rd': data[20],
        'wide_2nd3rd': data[21],
        'umatan': data[22],
        'sanrenfuku': data[23],
        'sanrentan': data[24] 
    }
    return outputs

def get_entries(race_id):
    # 接続する 
    con = MySQLdb.connect(
            user='user',
            passwd='secret',
            host='127.0.0.1',
            db='keiba_db',
            charset="utf8")
 
    # カーソルを取得する
    cur= con.cursor()

    # クエリを実行する
    sql = "select * from v_race_entries where race_id = '{}'".format(race_id)
    cur.execute(sql)

    # 実行結果をすべて取得する
    rows = cur.fetchall()
 
    cur.close()
    con.close()
    
    return rows

# 馬券購入シミュレーションを行う関数
def simulate_betting_simple(df, model, use_columns, normalize_params, tansho_flag=False, tansho_buy_num=3, wide_flag=False, umaren_flag=False, sanrenfuku=False):
    # 正規化されたデータの復元を行う関数
    def denormalize_value(normalized_value, param: dict):
        return normalized_value * param['sigma'] + param['avg']
    
    # データ処理部
    # 混合行列
    tp = 0
    fp = 0
    tn = 0
    fn = 0

    # 購入金額・払い戻し金額
    buy = 0
    ret = 0

    # 賭け統計データ
    win = 0
    min_odds = 0
    total_race_num = 0
    win_race_num = 0

    for race_id, group in df.groupby('race_id'):
        win_init = win
        # 全頭分の予想が得られないレースは参加しない
        num_entry = get_entries(race_id)
        if num_entry[0][1] != len(group):
            continue

        # オッズ情報取得
        rawdata = get_result_odds(race_id)
        result_odds = process_odds_data(rawdata[0])
        if 0 in result_odds.values() or len([x for x in result_odds.values() if x]) == 0:
            # オッズに０が含まれる場合正常に処理できないためスキップする
            continue
        # 順位格納用変数
        actual_top3 = {}
        predict_top3 = {}
        # 購入馬券格納用変数
        bettings = {
            'tansho': 0,
            'wide': 0,
            'umaren': 0,
            'umatan': 0,
            'sanrenfuku': 0
        }

        # レース毎のシミュレーションを行う
        total_race_num += 1
        test_sub_ds = utils.df_to_dataset(group.loc[:,use_columns], batch_size=1) # 出場馬データ
        y_model_sub = pd.DataFrame(denormalize_value(model.predict(test_sub_ds), normalize_params['race_time'])) # 予想タイム
        
        test_odds_sub = np.array(group['odds']) # 単勝オッズ
        test_rank_sub = np.array(group['rank']) # 実際の順位
        # 馬券購入シミュレーション：予想タイムにしたがってランク付けし、上位３馬の馬を購入
        for model_rank, actual_rank, odds, horse in zip(y_model_sub.rank(method='first')[0].to_numpy(), test_rank_sub, test_odds_sub, group.loc[:,'umaban'].to_numpy()):
            # 混合行列作成処理（上位3着）
            if actual_rank <= 3 and model_rank <= 3:
                tp += 1
            elif actual_rank > 3 and model_rank <= 3:
                fp += 1
            elif actual_rank <= 3 and model_rank > 3:
                fn += 1
            elif actual_rank > 3 and model_rank > 3:
                tn += 1

            # 予想上位3頭と結果上位３頭をそれぞれ記録
            if actual_rank <= 3:
                actual_top3[actual_rank] = horse
            if model_rank <= 3:
                predict_top3[model_rank] = horse

            if tansho_flag:
                # ルール①単勝購入：オッズが基準以上なら購入する
                criteria_tansho = 3.0 # 購入条件基準オッズ
                if model_rank <= tansho_buy_num and odds >= criteria_tansho:
                    bettings['tansho'] += 1
                    buy += 100
                    if actual_rank == 1:
                        ret += 100*odds
                        win += 1

        # ルール②ワイド購入
        if wide_flag:
            bettings['wide'] += 3
            buy += 300
            if predict_top3[1] in actual_top3.values() and predict_top3[2] in actual_top3.values():
                ret += 100*result_odds['wide_1st2nd']
                win += 1

            if predict_top3[1] in actual_top3.values() and predict_top3[3] in actual_top3.values():
                ret += 100*result_odds['wide_1st3rd']
                win += 1

            if predict_top3[2] in actual_top3.values() and predict_top3[3] in actual_top3.values():
                ret += 100*result_odds['wide_2nd3rd']
                win += 1
        
        #ルール③馬連購入：1・２・3位予想の組み合わせ（3通り）で購入する
        if umaren_flag:
            bettings['umaren'] += 3
            buy += 300
            if set([predict_top3[1], predict_top3[2]]) == set([actual_top3[1], actual_top3[2]]) or\
                set([predict_top3[1], predict_top3[3]]) == set([actual_top3[1], actual_top3[2]]) or\
                set([predict_top3[2], predict_top3[3]]) == set([actual_top3[1], actual_top3[2]]):
                ret += 100*result_odds['umaren']
                win += 1

        #ルール④三連複購入：
        if sanrenfuku:
            bettings['sanrenfuku'] += 1
            buy += 100
            if set(predict_top3.values()) == set(actual_top3.values()):
                ret += 100*result_odds['sanrenfuku']
                win += 1

        if win > win_init:
            win_race_num += 1

        # print('race_id:', race_id)
        # print(predict_top3[1], predict_top3[2], predict_top3[3], sep='\t')
        # print(actual_top3[1], actual_top3[2], actual_top3[3], sep='\t')

    print("勝ち馬券: {}, 外れ馬券: {}".format(tp, fp))
    print("機会損失: {}, 損失回避: {}".format(fn, tn))
    print("全レース数: {}, 勝ちレース数: {} ({:.0f}%)".format(total_race_num, win_race_num, win_race_num/total_race_num*100))
    if buy != 0 and win != 0:
        print("購入金額: {}円, 払戻金額: {:.0f}円, ROI: {:.2f}%, 平均オッズ: {:.2f}".format(buy, ret, ret/buy*100, ret/100/win))
    if tp+fp != 0 and tp+fn != 0:
        print("適合率: {:.1f}%（3着以内予想したうち実際に3着以内に入った馬）".format(tp/(tp+fp)*100))
        print("再現率: {:.1f}%（実際に3着以内に入った馬のうち3着以内予想していた馬）".format(tp/(tp+fn)*100))
        print("正解率: {:.1f}%".format((tp+tn)/(tp+tn+fp+fn)*100))
    # plt.scatter(model_ranks, actual_ranks)
    if buy == 0:
        ROI = 0
    else:
        ROI = ret/buy
    
    if win == 0:
        odds_avg = 0
    else:
        odds_avg = ret/100/win

    if tp+fp == 0:
        precision_score = 0
    else:
        precision_score = tp/(tp+fp)
    
    if tp+fn == 0:
        recall_score = 0
    else:
        recall_score = tp/(tp+fn)

    result = {
        'tp': tp, 'fp': fp, 'fn': fn, 'tn': tn,
        'total_race_num': total_race_num, 'win_race_num': win_race_num, 'win_race_ratio': win_race_num/total_race_num,
        'buy': buy, 'return': ret, 'ROI': ROI, 'odds_avg': odds_avg,
        'precision_score': precision_score, 'recall_score': recall_score, 'accuracy': (tp+tn)/(tp+tn+fp+fn)
    }
    return result

# 馬券購入シミュレーションを行う関数
def simulate_betting_pair_data(df, df_row, model, use_columns, tansho_flag=False, tansho_buy_num=3, wide_flag=False, umaren_flag=False, sanrenfuku=False, logfilename='./log/sim_result.log'):
    logging.basicConfig(filename=logfilename, level=logging.INFO)
    # データ処理部
    # 混合行列
    tp = 0
    fp = 0
    tn = 0
    fn = 0

    # 購入金額・払い戻し金額
    buy = 0
    ret = 0

    # 賭け統計データ
    win = 0
    total_race_num = 0
    win_race_num = 0

    for race_id, group in df.groupby('race_id'):
        win_init = win
        # 全頭分の予想が得られないレースは参加しない
        num_entry = get_entries(race_id)
        if num_entry[0][1]*(num_entry[0][1]-1) != len(group):
            continue

        # オッズ情報取得
        rawdata = get_result_odds(race_id)
        result_odds = process_odds_data(rawdata[0])
        if 0 in result_odds.values() or len([x for x in result_odds.values() if x]) == 0:
            # オッズに０が含まれる場合正常に処理できないためスキップする
            continue
        # 順位格納用変数
        actual_top3 = {}
        predict_top3 = {}
        # 購入馬券格納用変数
        bettings = {
            'tansho': 0,
            'wide': 0,
            'umaren': 0,
            'umatan': 0,
            'sanrenfuku': 0
        }

        # レース毎のシミュレーションを行う ここから修正が必要になる
        # シミュレーションは出場馬の各ペアの勝敗を計算して、その結果の順位付をする
        total_race_num += 1
        test_sub_ds = utils.df_to_dataset(group.loc[:,use_columns], batch_size=1) # 出場馬データ
        y_model_sub = model.predict(test_sub_ds) # 各組み合わせごとのタイム差((n,1)のndarray)

        score = {}
        for i in range(num_entry[0][1]):
            score[i+1] = 0.0
        j = 0
        for i, h in group.iterrows():
            score[int(h['A_umaban'])] += y_model_sub[j,0]
            logging.info(str(int(h['A_umaban']))+"-"+str(int(h['B_umaban']))+":"+str(y_model_sub[j,0]))
            j += 1
        
        # output score to log
        logging.info(score)

        # 予想スコアに基づく順位
        predict_score = pd.DataFrame(data={
            'umaban': score.keys(),
            'score': score.values()
        }, columns=['umaban', 'score'])

        predict_score = predict_score.sort_values('umaban')
        df_race_data = df_row[df_row['race_id'] == race_id].sort_values('umaban')
        test_odds_sub = np.array(df_race_data['odds']) # 単勝オッズ
        test_rank_sub = np.array(df_race_data['rank']) # 実際の順位
        # 馬券購入シミュレーション：予想タイムにしたがってランク付けし、上位３馬の馬を購入
        for model_rank, actual_rank, odds, horse in zip(predict_score['score'].rank(method='first').to_numpy(), test_rank_sub, test_odds_sub, predict_score['umaban'].to_numpy()):
            # 混合行列作成処理（上位3着）
            if actual_rank <= 3 and model_rank <= 3:
                tp += 1
            elif actual_rank > 3 and model_rank <= 3:
                fp += 1
            elif actual_rank <= 3 and model_rank > 3:
                fn += 1
            elif actual_rank > 3 and model_rank > 3:
                tn += 1

            # 予想上位3頭と結果上位３頭をそれぞれ記録
            if actual_rank <= 3:
                actual_top3[actual_rank] = horse
            if model_rank <= 3:
                predict_top3[model_rank] = horse

            if tansho_flag:
                # ルール①単勝購入：オッズが基準以上なら購入する
                criteria_tansho = 3.0 # 購入条件基準オッズ
                if model_rank <= tansho_buy_num and odds >= criteria_tansho:
                    bettings['tansho'] += 1
                    buy += 100
                    if actual_rank == 1:
                        ret += 100*odds
                        win += 1

        # ルール②ワイド購入
        if wide_flag:
            bettings['wide'] += 3
            buy += 300
            if predict_top3[1] in actual_top3.values() and predict_top3[2] in actual_top3.values():
                ret += 100*result_odds['wide_1st2nd']
                win += 1

            if predict_top3[1] in actual_top3.values() and predict_top3[3] in actual_top3.values():
                ret += 100*result_odds['wide_1st3rd']
                win += 1

            if predict_top3[2] in actual_top3.values() and predict_top3[3] in actual_top3.values():
                ret += 100*result_odds['wide_2nd3rd']
                win += 1
        
        #ルール③馬連購入：1・２・3位予想の組み合わせ（3通り）で購入する
        if umaren_flag:
            bettings['umaren'] += 3
            buy += 300
            if set([predict_top3[1], predict_top3[2]]) == set([actual_top3[1], actual_top3[2]]) or\
                set([predict_top3[1], predict_top3[3]]) == set([actual_top3[1], actual_top3[2]]) or\
                set([predict_top3[2], predict_top3[3]]) == set([actual_top3[1], actual_top3[2]]):
                ret += 100*result_odds['umaren']
                win += 1

        #ルール④三連複購入：
        if sanrenfuku:
            bettings['sanrenfuku'] += 1
            buy += 100
            if set(predict_top3.values()) == set(actual_top3.values()):
                ret += 100*result_odds['sanrenfuku']
                win += 1

        if win > win_init:
            win_race_num += 1

    print("勝ち馬券: {}, 外れ馬券: {}".format(tp, fp))
    print("機会損失: {}, 損失回避: {}".format(fn, tn))
    print("全レース数: {}, 勝ちレース数: {} ({:.0f}%)".format(total_race_num, win_race_num, win_race_num/total_race_num*100))
    if buy != 0 and win != 0:
        print("購入金額: {}円, 払戻金額: {:.0f}円, ROI: {:.2f}%, 平均オッズ: {:.2f}".format(buy, ret, ret/buy*100, ret/100/win))
    if tp+fp != 0 and tp+fn != 0:
        print("適合率: {:.1f}%（3着以内予想したうち実際に3着以内に入った馬）".format(tp/(tp+fp)*100))
        print("再現率: {:.1f}%（実際に3着以内に入った馬のうち3着以内予想していた馬）".format(tp/(tp+fn)*100))
        print("正解率: {:.1f}%".format((tp+tn)/(tp+tn+fp+fn)*100))
    # plt.scatter(model_ranks, actual_ranks)
    if buy == 0:
        ROI = 0
    else:
        ROI = ret/buy
    
    if win == 0:
        odds_avg = 0
    else:
        odds_avg = ret/100/win

    if tp+fp == 0:
        precision_score = 0
    else:
        precision_score = tp/(tp+fp)
    
    if tp+fn == 0:
        recall_score = 0
    else:
        recall_score = tp/(tp+fn)

    result = {
        'tp': tp, 'fp': fp, 'fn': fn, 'tn': tn,
        'total_race_num': total_race_num, 'win_race_num': win_race_num, 'win_race_ratio': win_race_num/total_race_num,
        'buy': buy, 'return': ret, 'ROI': ROI, 'odds_avg': odds_avg,
        'precision_score': precision_score, 'recall_score': recall_score, 'accuracy': (tp+tn)/(tp+tn+fp+fn)
    }
    return result

# 馬券購入シミュレーションを行う関数（ランダムフォレスト用）
def simulate_betting_simple_rf(df, model, use_columns, normalize_params, tansho_flag=False, tansho_buy_num=3, wide_flag=False, umaren_flag=False, sanrenfuku=False):
    # 正規化されたデータの復元を行う関数
    def denormalize_value(normalized_value, param: dict):
        return normalized_value * param['sigma'] + param['avg']
    
    # データ処理部
    # 混合行列
    tp = 0
    fp = 0
    tn = 0
    fn = 0

    # 購入金額・払い戻し金額
    buy = 0
    ret = 0

    # 賭け統計データ
    win = 0
    min_odds = 0
    total_race_num = 0
    win_race_num = 0

    for race_id, group in df.groupby('race_id'):
        win_init = win
        # 全頭分の予想が得られないレースは参加しない
        num_entry = get_entries(race_id)
        if num_entry[0][1] != len(group):
            continue

        # オッズ情報取得
        rawdata = get_result_odds(race_id)
        result_odds = process_odds_data(rawdata[0])
        if 0 in result_odds.values() or len([x for x in result_odds.values() if x]) == 0:
            # オッズに０が含まれる場合正常に処理できないためスキップする
            continue
        # 順位格納用変数
        actual_top3 = {}
        predict_top3 = {}
        # 購入馬券格納用変数
        bettings = {
            'tansho': 0,
            'wide': 0,
            'umaren': 0,
            'umatan': 0,
            'sanrenfuku': 0
        }

        # レース毎のシミュレーションを行う
        total_race_num += 1
        test_input = group.loc[:,use_columns] # 出場馬データ
        test_input.pop("target")
        test_predict = pd.DataFrame(denormalize_value(model.predict(test_input), normalize_params['race_time'])) # 予想タイム
        
        test_odds_sub = np.array(group['odds']) # 単勝オッズ
        test_rank_sub = np.array(group['rank']) # 実際の順位
        # 馬券購入シミュレーション：予想タイムにしたがってランク付けし、上位３馬の馬を購入
        for model_rank, actual_rank, odds, horse in zip(test_predict.rank()[0].to_numpy(), test_rank_sub, test_odds_sub, group.loc[:,'umaban'].to_numpy()):
            # 混合行列作成処理（上位3着）
            if actual_rank <= 3 and model_rank <= 3:
                tp += 1
            elif actual_rank > 3 and model_rank <= 3:
                fp += 1
            elif actual_rank <= 3 and model_rank > 3:
                fn += 1
            elif actual_rank > 3 and model_rank > 3:
                tn += 1

            # 予想上位3頭と結果上位３頭をそれぞれ記録
            if actual_rank <= 3:
                actual_top3[actual_rank] = horse
            if model_rank <= 3:
                predict_top3[model_rank] = horse

            if tansho_flag:
                # ルール①単勝購入：オッズが基準以上なら購入する
                criteria_tansho = 3.0 # 購入条件基準オッズ
                if model_rank <= tansho_buy_num and odds >= criteria_tansho:
                    bettings['tansho'] += 1
                    buy += 100
                    if actual_rank == 1:
                        ret += 100*odds
                        win += 1

        # ルール②ワイド購入
        if wide_flag:
            bettings['wide'] += 3
            buy += 300
            if predict_top3[1] in actual_top3.values() and predict_top3[2] in actual_top3.values():
                ret += 100*result_odds['wide_1st2nd']
                win += 1

            if predict_top3[1] in actual_top3.values() and predict_top3[3] in actual_top3.values():
                ret += 100*result_odds['wide_1st3rd']
                win += 1

            if predict_top3[2] in actual_top3.values() and predict_top3[3] in actual_top3.values():
                ret += 100*result_odds['wide_2nd3rd']
                win += 1
        
        #ルール③馬連購入：1・２・3位予想の組み合わせ（3通り）で購入する
        if umaren_flag:
            bettings['umaren'] += 3
            buy += 300
            if set([predict_top3[1], predict_top3[2]]) == set([actual_top3[1], actual_top3[2]]) or\
                set([predict_top3[1], predict_top3[3]]) == set([actual_top3[1], actual_top3[2]]) or\
                set([predict_top3[2], predict_top3[3]]) == set([actual_top3[1], actual_top3[2]]):
                ret += 100*result_odds['umaren']
                win += 1

        #ルール④三連複購入：
        if sanrenfuku:
            bettings['sanrenfuku'] += 1
            buy += 100
            if set(predict_top3.values()) == set(actual_top3.values()):
                ret += 100*result_odds['sanrenfuku']
                win += 1

        if win > win_init:
            win_race_num += 1

    print("勝ち馬券: {}, 外れ馬券: {}".format(tp, fp))
    print("機会損失: {}, 損失回避: {}".format(fn, tn))
    print("全レース数: {}, 勝ちレース数: {} ({:.0f}%)".format(total_race_num, win_race_num, win_race_num/total_race_num*100))
    if buy != 0 and win != 0:
        print("購入金額: {}円, 払戻金額: {:.0f}円, ROI: {:.2f}%, 平均オッズ: {:.2f}".format(buy, ret, ret/buy*100, ret/100/win))
    if tp+fp != 0 and tp+fn != 0:
        print("適合率: {:.1f}%（3着以内予想したうち実際に3着以内に入った馬）".format(tp/(tp+fp)*100))
        print("再現率: {:.1f}%（実際に3着以内に入った馬のうち3着以内予想していた馬）".format(tp/(tp+fn)*100))
        print("正解率: {:.1f}%".format((tp+tn)/(tp+tn+fp+fn)*100))

    if buy == 0:
        ROI = 0
    else:
        ROI = ret/buy
    
    if win == 0:
        odds_avg = 0
    else:
        odds_avg = ret/100/win

    if tp+fp == 0:
        precision_score = 0
    else:
        precision_score = tp/(tp+fp)
    
    if tp+fn == 0:
        recall_score = 0
    else:
        recall_score = tp/(tp+fn)

    result = {
        'tp': tp, 'fp': fp, 'fn': fn, 'tn': tn,
        'total_race_num': total_race_num, 'win_race_num': win_race_num, 'win_race_ratio': win_race_num/total_race_num,
        'buy': buy, 'return': ret, 'ROI': ROI, 'odds_avg': odds_avg,
        'precision_score': precision_score, 'recall_score': recall_score, 'accuracy': (tp+tn)/(tp+tn+fp+fn)
    }
    return result

def create_data_matrix(df, model, use_columns, normalize_params, tansho_flag=False, wide_flag=False, umaren_flag=False, sanrenfuku=False):
    # 混合行列(1~5着とそれ以下の順位)
    # 第1引数：1~5は予想順位、0は6着以下
    # 第2引数：1~5は結果順位、0は6着以下
    dataMatrix = np.zeros((6,6))

    # サンプル数
    num_elems = 0

    for race_id, group in df.groupby('race_id'):
        # 全頭分の予想が得られないレースは参加しない
        num_entry = get_entries(race_id)
        if num_entry[0][1] != len(group):
            continue

        # レース毎のシミュレーションを行う
        test_sub_ds = utils.df_to_dataset(group.loc[:,use_columns], batch_size=1) # 出場馬データ
        y_model_sub = pd.DataFrame(model.predict(test_sub_ds)) # 予想タイム指数

        test_rank_sub = np.array(group['rank']) # 実際の順位
        # 予想順位と実際の順位の分布行列を作成する
        for model_rank, actual_rank in zip(np.array(y_model_sub.rank()[0].to_numpy(), dtype='int64'), test_rank_sub):
            num_elems += 1
            if model_rank <= 5 and actual_rank <= 5:
                dataMatrix[model_rank][actual_rank] += 1
            elif model_rank <= 5 and actual_rank > 5:
                dataMatrix[model_rank][0] += 1
            elif model_rank > 5 and actual_rank <= 5:
                dataMatrix[0][actual_rank] += 1
            else:
                dataMatrix[0][0] += 1

    dataMatrix_df = pd.DataFrame(dataMatrix)
    rowsum = dataMatrix_df.sum(axis='columns')

    probability = dataMatrix_df.div(rowsum, axis=0)
    # それぞれの予想順位による条件付き確率
    print(probability)

    # 全確率
    print(dataMatrix_df/num_elems)
    return dataMatrix_df

def create_rank_matrix_all(df, model, use_columns):
    # 混合行列
    # 第1引数：予想順位
    # 第2引数：結果順位
    dataMatrix = np.zeros((19,19))

    for race_id, group in df.groupby('race_id'):
        # 全頭分の予想が得られないレースは参加しない
        num_entry = get_entries(race_id)
        if num_entry[0][1] != len(group):
            continue

        # レース毎のシミュレーションを行う
        test_sub_ds = utils.df_to_dataset(group.loc[:,use_columns], batch_size=1) # 出場馬データ
        y_model_sub = pd.DataFrame(model.predict(test_sub_ds)) # 予想タイム指数

        test_rank_sub = np.array(group['rank']) # 実際の順位
        # 予想順位と実際の順位の分布行列を作成する
        for model_rank, actual_rank in zip(np.array(y_model_sub.rank()[0].to_numpy(), dtype='int64'), test_rank_sub):
            dataMatrix[model_rank][actual_rank] += 1

    dataMatrix_df = pd.DataFrame(dataMatrix)

    return dataMatrix_df