# 学習時に汎用的に使用される関数モジュールです
import tensorflow as tf
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
import MySQLdb
import time
import pandas as pd
import datetime
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rcParams['figure.figsize'] = (12, 10)
colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

def select_data(sql):
    # 接続する 
    con = MySQLdb.connect(
            user='user',
            passwd='secret',
            host='127.0.0.1',
            db='keiba_db',
            charset="utf8")
 
    # カーソルを取得する
    cur= con.cursor()
    cur.execute(sql)

    # 実行結果をすべて取得する
    rows = cur.fetchall()
 
    cur.close()
    con.close()
    
    return rows

# データの初期処理のためのメソッド
def initial_process_row_data(data, columns, normalize_params):
    # データの正規化・復元を行う関数
    def normalize_value(raw_val, param: dict):
        return (raw_val - param['avg'])/param['sigma']
    
    # データ処理部
    df = pd.DataFrame(data, columns=columns)
    df = df.assign(
        weight = normalize_value(df['weight'], normalize_params['weight']),
        d_weight = normalize_value(df['d_weight'], normalize_params['d_weight']),
        racing_weight = normalize_value(df['racing_weight'], normalize_params['racing_weight']),
        distance = normalize_value(df['distance'], normalize_params['distance']), # 距離を追加
        weight_pre_1 = normalize_value(df['weight_pre_1'], normalize_params['weight']),
        weight_pre_2 = normalize_value(df['weight_pre_2'], normalize_params['weight']),
        weight_pre_3 = normalize_value(df['weight_pre_3'], normalize_params['weight']),
        weight_pre_4 = normalize_value(df['weight_pre_4'], normalize_params['weight']),
        weight_pre_5 = normalize_value(df['weight_pre_5'], normalize_params['weight']),
        weight_change_pre_1 = normalize_value(df['weight_change_pre_1'], normalize_params['d_weight']),
        weight_change_pre_2 = normalize_value(df['weight_change_pre_2'], normalize_params['d_weight']),
        weight_change_pre_3 = normalize_value(df['weight_change_pre_3'], normalize_params['d_weight']),
        weight_change_pre_4 = normalize_value(df['weight_change_pre_4'], normalize_params['d_weight']),
        weight_change_pre_5 = normalize_value(df['weight_change_pre_5'], normalize_params['d_weight']),
        distance_pre_1 = normalize_value(df['distance_pre_1'], normalize_params['distance']),
        distance_pre_2 = normalize_value(df['distance_pre_2'], normalize_params['distance']),
        distance_pre_3 = normalize_value(df['distance_pre_3'], normalize_params['distance']),
        distance_pre_4 = normalize_value(df['distance_pre_4'], normalize_params['distance']),
        distance_pre_5 = normalize_value(df['distance_pre_5'], normalize_params['distance']),
        time_3f_pre_1 = pd.DataFrame(map(lambda x:x.total_seconds(), df['time_3f_pre_1'])),
        time_3f_pre_2 = pd.DataFrame(map(lambda x:x.total_seconds(), df['time_3f_pre_2'])),
        time_3f_pre_3 = pd.DataFrame(map(lambda x:x.total_seconds(), df['time_3f_pre_3'])),
        time_3f_pre_4 = pd.DataFrame(map(lambda x:x.total_seconds(), df['time_3f_pre_4'])),
        time_3f_pre_5 = pd.DataFrame(map(lambda x:x.total_seconds(), df['time_3f_pre_5'])),
        target = pd.DataFrame(map(lambda x:normalize_value(x.total_seconds(), normalize_params['race_time']), df['race_time']))
    )
    return df

# データの初期処理のためのメソッド
def initial_process_row_data_rf(data, columns, normalize_params, le):
    # データの正規化・復元を行う関数
    def normalize_value(raw_val, param: dict):
        return (raw_val - param['avg'])/param['sigma']
    
    # データ処理部
    df = pd.DataFrame(data, columns=columns)
    df = df.assign(
        weight = normalize_value(df['weight'], normalize_params['weight']),
        d_weight = normalize_value(df['d_weight'], normalize_params['d_weight']),
        racing_weight = normalize_value(df['racing_weight'], normalize_params['racing_weight']),
        distance = normalize_value(df['distance'], normalize_params['distance']), # 距離を追加
        weight_pre_1 = normalize_value(df['weight_pre_1'], normalize_params['weight']),
        weight_pre_2 = normalize_value(df['weight_pre_2'], normalize_params['weight']),
        weight_pre_3 = normalize_value(df['weight_pre_3'], normalize_params['weight']),
        weight_pre_4 = normalize_value(df['weight_pre_4'], normalize_params['weight']),
        weight_pre_5 = normalize_value(df['weight_pre_5'], normalize_params['weight']),
        weight_change_pre_1 = normalize_value(df['weight_change_pre_1'], normalize_params['d_weight']),
        weight_change_pre_2 = normalize_value(df['weight_change_pre_2'], normalize_params['d_weight']),
        weight_change_pre_3 = normalize_value(df['weight_change_pre_3'], normalize_params['d_weight']),
        weight_change_pre_4 = normalize_value(df['weight_change_pre_4'], normalize_params['d_weight']),
        weight_change_pre_5 = normalize_value(df['weight_change_pre_5'], normalize_params['d_weight']),
        distance_pre_1 = normalize_value(df['distance_pre_1'], normalize_params['distance']),
        distance_pre_2 = normalize_value(df['distance_pre_2'], normalize_params['distance']),
        distance_pre_3 = normalize_value(df['distance_pre_3'], normalize_params['distance']),
        distance_pre_4 = normalize_value(df['distance_pre_4'], normalize_params['distance']),
        distance_pre_5 = normalize_value(df['distance_pre_5'], normalize_params['distance']),
        time_3f_pre_1 = pd.DataFrame(map(lambda x:x.total_seconds(), df['time_3f_pre_1'])),
        time_3f_pre_2 = pd.DataFrame(map(lambda x:x.total_seconds(), df['time_3f_pre_2'])),
        time_3f_pre_3 = pd.DataFrame(map(lambda x:x.total_seconds(), df['time_3f_pre_3'])),
        time_3f_pre_4 = pd.DataFrame(map(lambda x:x.total_seconds(), df['time_3f_pre_4'])),
        time_3f_pre_5 = pd.DataFrame(map(lambda x:x.total_seconds(), df['time_3f_pre_5'])),
        place = le['place'].transform(df['place']),
        surface = le['surface'].transform(df['surface']),
        direction = le['direction'].transform(df['direction']),
        sex = le['sex'].transform(df['sex']),
        weather = le['weather'].transform(df['weather']),
        surface_condition = le['surface_condition'].transform(df['surface_condition']),
        target = pd.DataFrame(map(lambda x:normalize_value(x.total_seconds(), normalize_params['race_time']), df['race_time']))
    )
    return df

# Pandasデータフレームからtf.dataデータセットを作るためのユーティリティメソッド
def df_to_dataset(dataframe, shuffle=False, batch_size=32):
    dataframe = dataframe.copy()
    labels = dataframe.pop('target')
    ds = tf.data.Dataset.from_tensor_slices((dict(dataframe), labels))
    if shuffle:
        ds = ds.shuffle(buffer_size=len(dataframe))
    ds = ds.batch(batch_size)
    return ds

def plot_loss(history, label, n):
    # Use a log scale on y-axis to show the wide range of values.
    plt.semilogy(history.epoch, history.history['loss'],color=colors[n], label='Train ' + label)
    plt.semilogy(history.epoch, history.history['val_loss'],
               color=colors[n], label='Val ' + label,
               linestyle="--")
    plt.xlabel('Epoch')
    plt.ylabel('Loss')